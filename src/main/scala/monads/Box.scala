package monads

case class Box[A](a: A)

object Box {
  implicit object BoxMonad extends Monad[Box] {
    def point[A](a: A): Box[A] = Box(a)

    override
    def bind[A, B](fa: Box[A])(f: A => Box[B]): Box[B] =
      f(fa.a)
  }

  import Monad._

  def example = 
    for {
      a <- Box(1)
      b <- Box(a * 2)
      c <- Box(b - 100)
    } yield c

  val unbox = Monad[Box].join(Box(Box(1)))
}