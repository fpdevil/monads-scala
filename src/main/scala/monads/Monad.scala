package monads

trait Monad[F[_]] extends Functor[F] {
  def point[A](a: A): F[A]

  def bind[A, B](fa: F[A])(f: A => F[B]): F[B] =
    join(map(fa)(f))

  def join[A](fa: F[F[A]]): F[A] =
    bind(fa)(identity) 

  override
  def map[A, B](fa: F[A])(f: A => B): F[B] =
    bind(fa) { f andThen point[B] }
}

object Monad {
  def apply[F[_]: Monad]: Monad[F] =
    implicitly[Monad[F]]

  implicit class MonadOps[F[_]: Monad, A](fa: F[A]) {
    def map[B](f: A => B): F[B] =
      Monad[F].map(fa)(f)

    def flatMap[B](f: A => F[B]): F[B] =
      Monad[F].bind(fa)(f)

    def join(implicit ev: F[A] <:< F[F[A]]): F[A] =
      Monad[F].join(fa)
  }
}
