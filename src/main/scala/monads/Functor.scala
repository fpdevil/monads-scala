package monads

trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B] =
    fmap(fa)(f)

  def fmap[A, B]: F[A] => (A => B) => F[B] =
    fa => f => map(fa)(f)

  def flipmap[A, B]: (A => B) => F[A] => F[B] =
    f => fa => map(fa)(f)
}

object Functor {
  def apply[F[_]: Functor]: Functor[F] =
    implicitly[Functor[F]]

  implicit object ListFunctor extends Functor[List] {
    override 
    def fmap[A, B]: List[A] => (A => B) => List[B] =
      as => f => (for (a <- as) yield f(a)).reverse
  }

  implicit object OptionFunctor extends Functor[Option] {
    override 
    def map[A, B](opt: Option[A])(f: A => B): Option[B] =
      opt match {
        case Some(a) => Some(f(a))
        case None    => None
      }
  }
}