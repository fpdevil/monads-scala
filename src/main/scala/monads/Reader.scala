package monads

case class Reader[R, A](run: R => A)

object Reader {
  
  def ask[A]: Reader[A, A]= Reader(identity)

  class Reader_[R] {
    type l[a] = Reader[R, a]
  }

  implicit def ReaderMonad[R]: Monad[Reader_[R]#l] =
    new Monad[Reader_[R]#l] {
  // implicit def ReaderMonad[R]: Monad[Reader[R, ?]] =
  //   new Monad[Reader[R, ?]] {
      def point[A](a: A): Reader[R, A] = 
        Reader(_ => a)

      override
      def bind[A, B](fa: Reader[R, A])(f: A => Reader[R, B]): Reader[R, B] =
        Reader[R, B](r => f(fa.run(r)).run(r))
    }
}